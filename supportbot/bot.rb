module SupportBot
  class Bot < SlackRubyBot::Bot
    MAX_SLACK_RETRIES = 5

    def self.run
      retry_count = 0
      Thread.new { SupportBot::Commands::SLAH.run_slah_loop }

      loop do
        begin
          break super
        rescue Slack::Web::Api::Errors::TooManyRequestsError => e
          raise e if retry_count >= MAX_SLACK_RETRIES

          STDERR.puts "ERROR: #{e}"
          STDERR.puts "Retrying: #{retry_count}/#{MAX_SLACK_RETRIES}"
          retry_count += 1
          sleep(e.retry_after.seconds)
        end
      end
    end
  end
end
