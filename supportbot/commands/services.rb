require_relative './base'

module SupportBot
  module Commands
    class Services < Base
      # TODO: Is this still required?
      command 'services', 's'
      views :need_org_triage, :dot_com_with_sla, :sm_with_sla, :upgrade_renewals_ar
    end
  end
end
