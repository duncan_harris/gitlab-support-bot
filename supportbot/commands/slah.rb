require 'helpers/zendesk_helper'
require 'terminal-table'

module SupportBot
  module Commands
    class SLAH < SlackRubyBot::Commands::Base
      include ZendeskHelper

      # TODO: Move this within the expression to make it configurable
      SLAH_INTERVAL = 15 * 60 # seconds
      SLAH_CHANNEL = 'C4Y5DRKLK' # support_self-managed
      SLAH_CHANNEL_RESPONSE_CREW = 'C01AEL6E207' # support_response-crew

      command 'slah'

      def self.call(client, data, match)
        ZendeskHelper.slah_query if match[:expression] == 'status'

        client.say channel: data.channel, text: slah_status
      rescue StandardError => e
        client.say(channel: data.channel, text: "Sorry, #{e.message}.")
      end

      def self.slah_status
        'SLAH is active and running'
      end

      def self.run_slah_loop
        @logger = zd_client.config.logger
        client = SlackRubyBot::Client.new

        loop do
          sleep SLAH_INTERVAL

          tickets = ZendeskHelper.slah_query
          next if tickets.empty?

          ticket_ids = []
          tickets.each do |ticket|
            @logger.debug("reporting tickets #{ticket.id}, #{SLAH_CHANNEL}")

            text = "*[SLAH] under 2hrs until breach*\n" \
                   "Ticket ##{ticket.id} -#{ticket.subject}\n" \
                   "<https://gitlab.zendesk.com/agent/tickets/#{ticket.id}>"

            client.web_client.chat_postMessage(channel: SLAH_CHANNEL, text: text,
                                               unfurl_links: false, unfurl_media: false)

            # Temp: Used to report both messages in the other channel.
            client.web_client.chat_postMessage(channel: SLAH_CHANNEL_RESPONSE_CREW, text: text,
                                               unfurl_links: false, unfurl_media: false)
            ticket_ids << ticket.id
          end

          ZendeskHelper.update_tag(ticket_ids) unless ticket_ids.empty?
        end
      end
    end
  end
end
