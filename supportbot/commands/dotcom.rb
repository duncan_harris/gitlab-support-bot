require_relative './base'

module SupportBot
  module Commands
    class Dotcom < Base
      command 'dotcom', 'dc'
      views :need_org_triage, :dot_com_with_sla
    end
  end
end
