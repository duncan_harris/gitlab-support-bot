require 'helpers/zendesk_helper'
require 'terminal-table'

module SupportBot
  module Commands
    class Search < SlackRubyBot::Commands::Base
      include ZendeskHelper
      command 'search', 'find', 'grep'
      def self.call(client, data, match)
        results = ZendeskHelper.search_for(match[:expression])
        client.say channel: data.channel, text: response_table(results, match[:expression])
      rescue StandardError => e
        client.say(channel: data.channel, text: "Sorry, #{e.message}.")
      end

      def self.response_table(results, search_string)
        table = Terminal::Table.new do |t|
          t.title = "FIRST #{results.count} SEARCH RESULTS FOR '#{search_string}'"
          results.each do |ticket|
            org = (ticket.organization ? ticket.organization.name : 'N/A')
            ticket_link = "https://gitlab.zendesk.com/agent/tickets/#{ticket.id}"
            status = ticket.status.try(:capitalize)
            support_level = ZendeskHelper.ticket_support_level(ticket)
            t.add_row ["#{ticket.subject}\n#{org}\n#{status} | #{support_level} | #{ticket_link}"]
          end
          t.style = { all_separators: true, alignment: :left }
        end

        "```\n#{table}\n```"
      end
    end
  end
end
