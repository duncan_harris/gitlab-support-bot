module SupportBot
  module Commands
    class Hello < SlackRubyBot::Commands::Base
      # TODO: Add commands for morning, afternoon and goodbye
      command 'hi' do |client, data, _match|
        client.say(channel: data.channel, text: "Hi <@#{data.user}>!")
      end
    end
  end
end
