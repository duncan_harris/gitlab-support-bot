require 'fixtures/tickets'
require 'fixtures/views'

module SharedContext
  extend RSpec::SharedContext

  let(:ticket_counts) { TICKET_COUNTS }
  let(:mock_views) { MOCK_VIEWS }
end

module CommandsHelper
  extend RSpec::SharedContext

  before do
    allow(ZendeskHelper).to receive(:tickets_from_views).and_return(ticket_counts)
    allow(ZendeskHelper).to receive(:views).and_return(mock_views)
  end
end

RSpec.configure do |config|
  config.before do
    SlackRubyBot.config.user = 'sb'

    config.include SharedContext
  end
end
