require 'spec_helper'

describe ZendeskHelper do
  subject(:app) { ZendeskHelper }

  let!(:zd_client) { double }

  before do
    allow(app).to receive(:zd_client).and_return(zd_client)
  end

  context 'Views'

  context 'Search' do
    context '#search_for' do
      it 'runs a zd search' do
        expected_query = 'test_query type:ticket status:new status:open status:pending order_by:updated_at sort:desc'

        expect(zd_client).to receive(:search).with(per_page: 10, query: expected_query)

        app.search_for('test_query')
      end

      it 'runs a zd search with custom per_page setting' do
        expected_query = 'test_query type:ticket status:new status:open status:pending order_by:updated_at sort:desc'

        expect(zd_client).to receive(:search).with(per_page: 200, query: expected_query)

        app.search_for('test_query', 200)
      end
    end

    context '#ticket_support_level' do
      it 'returns N/A when the org does not exist' do
        result = app.ticket_support_level(mock_ticket)

        expect(result).to eq('N/A')
      end

      it 'returns the Support Level when it exists' do
        basic = app.ticket_support_level(mock_ticket('basic'))
        premium = app.ticket_support_level(mock_ticket('premium'))
        silver = app.ticket_support_level(mock_ticket('silver'))
        gold = app.ticket_support_level(mock_ticket('gold'))

        expect(basic).to eq('Basic')
        expect(premium).to eq('Premium')
        expect(silver).to eq('Silver')
        expect(gold).to eq('Gold')
      end
    end
  end

  context 'SLAH query' do
    let(:zd_config) { double }
    let(:zd_tickets) { double }

    before do
      allow(zd_config).to receive(:logger).and_return(Logger.new(nil))
      allow(zd_client).to receive(:config).and_return(zd_config)
      allow(zd_client).to receive(:tickets).and_return(zd_tickets)
      allow(zd_tickets).to receive(:show_many)
    end

    it 'runs a search' do
      expected_query = 'type:ticket status:open status:new tags:ultimate tags:premium tags:basic tags:starter'

      expect(zd_client).to receive(:search).with(query: expected_query, reload: true).and_return([])

      subject.slah_query
    end

    context 'with tickets returned' do
      before do
        allow(zd_client).to receive(:search).and_return([])
        allow(zd_tickets).to receive(:show_many).and_return(mock_tickets)
      end

      it 'filters out tickets without SLA' do
        allow(subject).to receive(:find_active_slah) do |ticket|
          next unless ticket.tags.map(&:id).include?('tag1') # mock sla

          ticket
        end

        tickets = subject.slah_query

        tickets.each do |ticket|
          # we're testing the general flow here, therefore
          # mocking the SLAH filter check instead to:
          # "tickets that don't have the `tag1` tag"
          expect(ticket.tags.map(&:id).include?('tag1')).to be_falsy
        end
      end
    end
  end

  private

  def mock_tickets
    statuses = %w('new', 'open', 'pending', 'hold', 'solved', 'closed')
    tags = %w('tag1', 'tag2')

    statuses.map do |status|
      tags.map do |tag|
        ZendeskAPI::Ticket.new(app, status: status, tags: [tag], sla: {})
      end
    end.flatten
  end

  def mock_ticket(support_level = nil)
    mock_ticket = ZendeskAPI::Ticket.new(app) # doesn't actually create it until `.save` is called
    return mock_ticket if support_level.nil?

    mock_ticket.organization = ZendeskAPI::Organization.new(app)
    mock_ticket.organization.organization_fields = { support_level: "#{support_level}" }
    mock_ticket
  end
end
