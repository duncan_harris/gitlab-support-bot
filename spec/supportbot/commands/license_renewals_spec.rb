require 'spec_helper'

describe SupportBot::Commands::LicenseRenewals do
  include CommandsHelper

  subject(:app) { SupportBot::Bot.instance }

  it_behaves_like 'a slack ruby bot'

  context 'the commands' do
    it 'returns license&renewals tickets' do
      expect(message: "#{SlackRubyBot.config.user} license-renewals", channel: 'channel').to respond_with_slack_message(expected_response)
    end

    it 'returns lr tickets' do
      expect(message: "#{SlackRubyBot.config.user} lr", channel: 'channel').to respond_with_slack_message(expected_response)
    end
  end

  def expected_response
    lr = ticket_counts[:upgrade_renewals_ar]

    /License & Renewals: \*#{lr}\*/
  end
end
