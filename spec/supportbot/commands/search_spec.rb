require 'spec_helper'

describe SupportBot::Commands::Search do
  include CommandsHelper

  subject(:app) { SupportBot::Bot.instance }

  it_behaves_like 'a slack ruby bot'

  before do
    allow(ZendeskHelper).to receive(:search_for).and_return([])
  end

  context 'the commands' do
    it 'returns the expected results' do
      allow(ZendeskHelper).to receive(:search_for)
        .and_return(
          [mock_ticket('geo')],
          [mock_ticket('Rack attack')],
          [mock_ticket('Rack attack')]
        )

      expect(message: "#{SlackRubyBot.config.user} search geo", channel: 'channel')
        .to respond_with_slack_message(response_table('geo'))
      expect(message: "#{SlackRubyBot.config.user} find Rack attack", channel: 'channel')
        .to respond_with_slack_message(response_table('Rack attack'))
      expect(message: "#{SlackRubyBot.config.user} grep Rack attack", channel: 'channel')
        .to respond_with_slack_message(response_table('Rack attack'))
    end

    it 'returns correctly when there are no search results' do
      table = Terminal::Table.new do |t|
        t.title = "FIRST 0 SEARCH RESULTS FOR 'abracadabra'"
        t.style = { all_separators: true, alignment: :left }
      end

      expect(message: "#{SlackRubyBot.config.user} find abracadabra", channel: 'channel')
        .to respond_with_slack_message("```\n#{table}\n```")
    end

    it 'returns correctly without an org' do
      allow(ZendeskHelper).to receive(:search_for).and_return([mock_ticket('organization:none')])

      expect(message: "#{SlackRubyBot.config.user} find organization:none", channel: 'channel')
        .to respond_with_slack_message(response_table('organization:none'))
    end
  end

  def mock_ticket(subject)
    ticket = ZendeskAPI::Ticket.new(ZendeskHelper, subject: subject)
    ticket.organization = ZendeskAPI::Organization.new(ZendeskHelper, name: 'mock')
    ticket.organization.organization_fields = { support_level: 'level' }
    ticket
  end

  def response_table(query) # rubocop:disable Metrics/LineLength
    results = [mock_ticket(query)]
    # results = ZendeskHelper.search_for(query)
    table = Terminal::Table.new do |t|
      t.title = "FIRST #{results.count} SEARCH RESULTS FOR '#{query}'"
      results.each do |ticket|
        org = (ticket.organization ? ticket.organization.name : 'N/A')
        ticket_link = "https://gitlab.zendesk.com/agent/tickets/#{ticket.id}"
        status = ticket.status.try(:capitalize)
        support_level = ZendeskHelper.ticket_support_level(ticket)
        t.add_row ["#{ticket.subject}\n#{org}\n#{status} | #{support_level} | #{ticket_link}"]
      end
      t.style = { all_separators: true, alignment: :left }
    end

    "```\n#{table}\n```"
  end
end
