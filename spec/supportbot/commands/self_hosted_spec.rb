require 'spec_helper'

describe SupportBot::Commands::SelfHosted do
  include CommandsHelper

  subject(:app) { SupportBot::Bot.instance }

  it_behaves_like 'a slack ruby bot'

  context 'the commands' do
    it 'returns self-hosted tickets' do
      expected_responses.each do |response|
        expect(message: "#{SlackRubyBot.config.user} self-hosted", channel: 'channel').to respond_with_slack_message(response)
      end
    end

    it 'returns sh tickets' do
      expected_responses.each do |response|
        expect(message: "#{SlackRubyBot.config.user} sh", channel: 'channel').to respond_with_slack_message(response)
      end
    end

    it 'returns self-managed tickets' do
      expected_responses.each do |response|
        expect(message: "#{SlackRubyBot.config.user} self-managed", channel: 'channel').to respond_with_slack_message(response)
      end
    end

    it 'returns sm tickets' do
      expected_responses.each do |response|
        expect(message: "#{SlackRubyBot.config.user} sm", channel: 'channel').to respond_with_slack_message(response)
      end
    end
  end

  def expected_responses
    triage = ticket_counts[:need_org_triage]
    sm_with_sla = ticket_counts[:sm_with_sla]
    total = triage + sm_with_sla

    [
      /Needs Org & Triage: \*#{triage}\*\n/,
      /Self-Managed with SLA: \*#{sm_with_sla}\*\n/,
      /Total: \*#{total}\*/
    ]
  end
end
