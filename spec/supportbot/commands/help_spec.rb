require 'spec_helper'

describe SupportBot::Commands::Help do
  include CommandsHelper

  subject(:app) { SupportBot::Bot.instance }

  it_behaves_like 'a slack ruby bot'

  it 'returns as expected' do
    expect(message: "#{SlackRubyBot.config.user} help", channel: 'channel').to respond_with_slack_message(response_table)
    expect(message: "#{SlackRubyBot.config.user} h", channel: 'channel').to respond_with_slack_message(response_table)
  end

  private

  def response_table
    # rubocop:disable Metrics/LineLength
    table = Terminal::Table.new do |t|
      t.title = "SUPPORTBOT HELP (#{SupportBot::Commands::Help::PROJECT_LOCATION})"

      t.headings = ['COMMAND(S)', 'DESCRIPTION']
      t.add_row ['dotcom, dc', 'Only shows the amount of tickets for gitlab.com (subscribers, trials, free)']
      t.add_row ['find, search, grep', "Search for a query among new, open, and pending tickets.\nReturns the first #{ZendeskHelper::PER_PAGE} results. Use it in the following\nformat:\n   sb find rack attack"]
      t.add_row ['help, h', 'Learn the various commands available through `sb <command>`']
      t.add_row ['hi', 'A warm greeting']
      t.add_row ['pods, p', 'Shows the amount of tickets for every view available in Zendesk']
      t.add_row ['self-hosted, sh, sm', 'Only shows the amount of tickets for self-managed']
      t.add_row ['services, s', 'Only shows the amount of tickets for gitlab.com']
      t.add_row ['slah', 'on/off to let the bot start watching the queue']
      t.add_row ['version', "Returns the version that SupportBot is running on #{SupportBot::VERSION}"]
      t.style = { all_separators: true, alignment: :left }
    end
    "```\n#{table}```"
    # rubocop:enable Metrics/LineLength
  end
end
