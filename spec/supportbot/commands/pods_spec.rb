require 'spec_helper'

describe SupportBot::Commands::Pods do
  include CommandsHelper

  subject(:app) { SupportBot::Bot.instance }

  it_behaves_like 'a slack ruby bot'

  context 'the commands' do
    it 'returns pods tickets' do
      expected_responses.each do |response|
        expect(message: "#{SlackRubyBot.config.user} pods", channel: 'channel').to respond_with_slack_message(response)
      end
    end

    it 'returns p tickets' do
      expected_responses.each do |response|
        expect(message: "#{SlackRubyBot.config.user} p", channel: 'channel').to respond_with_slack_message(response)
      end
    end
  end

  def expected_responses
    triage = ticket_counts[:need_org_triage]
    dot_com_with_sla = ticket_counts[:dot_com_with_sla]
    sm_with_sla = ticket_counts[:sm_with_sla]
    lr = ticket_counts[:upgrade_renewals_ar]
    total = triage + dot_com_with_sla + sm_with_sla + lr

    [
      /Needs Org & Triage: \*#{triage}\*\n/,
      /GitLab.com with SLA: \*#{dot_com_with_sla}\*\n/,
      /Self-Managed with SLA: \*#{sm_with_sla}\*\n/,
      /License & Renewals: \*#{lr}\*\n/,
      /Total: \*#{total}\*/
    ]
  end
end
