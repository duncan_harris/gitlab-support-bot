require 'spec_helper'

describe SupportBot::Commands::SLAH do
  include CommandsHelper

  subject(:app) { SupportBot::Bot.instance }

  it_behaves_like 'a slack ruby bot'

  before do
    allow(ZendeskHelper).to receive(:slah_query)
  end

  context 'the commands' do
    it 'returns slah status without the extra check' do
      expect(message: "#{SlackRubyBot.config.user} slah test_non_existing_subcommand",
             channel: 'channel').to respond_with_slack_message('SLAH is active and running')

      expect(ZendeskHelper).not_to have_received(:slah_query)
    end

    it 'returns slah status with the extra check' do
      expect(message: "#{SlackRubyBot.config.user} slah status",
             channel: 'channel').to respond_with_slack_message('SLAH is active and running')

      expect(ZendeskHelper).to have_received(:slah_query)
    end
  end
end
