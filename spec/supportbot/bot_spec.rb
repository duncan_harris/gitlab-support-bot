require 'spec_helper'

describe SupportBot::Bot do
  describe 'run' do
    it 'calls parent run method' do
      allow(SlackRubyBot::Bot).to receive(:run)

      SupportBot::Bot.run

      expect(SlackRubyBot::Bot).to have_received(:run).once
    end

    context 'when Slack rate-limits the request' do
      let(:error) { Slack::Web::Api::Errors::TooManyRequestsError.new(nil) }

      before do
        allow(SlackRubyBot::Bot).to receive(:sleep)
        allow(error).to receive(:retry_after).and_return(42)
      end

      it 'sleeps the amount returned by Slack' do
        # raise with sleep then return
        responses = [:raise, true]
        allow(SlackRubyBot::Bot).to receive(:run) do
          v = responses.shift
          v == :raise ? fail(error) : v
        end

        SupportBot::Bot.run

        expect(SlackRubyBot::Bot).to have_received(:sleep).with(42).once
      end

      it 'retries a limited amount of times' do
        allow(SlackRubyBot::Bot).to receive(:run).and_raise(error)

        expect { SupportBot::Bot.run }.to raise_error(Slack::Web::Api::Errors::TooManyRequestsError)

        expect(SlackRubyBot::Bot).to have_received(:sleep)
          .with(42).exactly(SupportBot::Bot::MAX_SLACK_RETRIES).times
      end
    end
  end
end
