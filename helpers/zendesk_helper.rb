require 'zendesk_api'
require 'json'

module ZendeskHelper
  require_relative '../services/zendesk'
  PER_PAGE = 10
  SLA_BREACH_TIME = 2.hours

  class ZendeskView
    attr_reader :type, :name, :id

    def initialize(type, name, id)
      @type = type
      @name = name
      @id = id
    end
  end

  def self.count_many
    view_ids = views.keys.map { |v| views[v].id }.join(',')

    is_fresh = false
    while is_fresh == false
      fresh_arr = []
      output_hash = {}
      api_response = zd_client.view_counts(ids: view_ids, path: 'views/count_many', reload: true).fetch
      api_response.each do |json_views|
        fresh_arr.push(json_views['fresh'])
        zendesk_view_key = views.keys.find { |v| views[v].id == json_views['view_id'] }
        output_hash[zendesk_view_key] = json_views['value']
      end
      is_fresh = (fresh_arr.uniq == [true])
    end
    output_hash
  end

  def self.tickets_from_views
    ZendeskHelper.count_many
  end

  def self.views
    return @views unless @views.nil?

    @views = {}
    [
      [:need_org_triage, 'Needs Org & Triage', 360_038_101_960],
      [:dot_com_with_sla, 'GitLab.com with SLA', 360_038_122_959],
      [:sm_with_sla, 'Self-Managed with SLA', 360_038_124_139],
      [:upgrade_renewals_ar, 'License & Renewals', 360_038_103_700]
    ].each do |view_data|
      view = ZendeskView.new(*view_data)
      @views[view.type] = view
    end

    @views
  end

  def self.search_for(query, per_page = PER_PAGE)
    query = "#{query} type:ticket status:new status:open status:pending order_by:updated_at sort:desc"
    results = zd_client.search(query: query, per_page: per_page)
    results.to_a
  end

  def self.ticket_support_level(ticket)
    return 'N/A' if ticket.organization.nil?
    return 'N/A' unless ticket.organization.organization_fields[:support_level]
    ticket.organization.organization_fields[:support_level].try(:capitalize)
  end

  def self.slah_query
    @logger = zd_client.config.logger
    @logger.debug '== Collection ticket information =='
    query = 'type:ticket status:open status:new tags:ultimate tags:premium tags:basic tags:starter'
    results = zd_client.search(query: query, reload: true)
    ticket_ids = results.to_a.map(&:id)

    tickets = zd_client.tickets.show_many(ids: ticket_ids, include: 'slas').to_a

    @logger.info "ticket count #{tickets.count}"

    remaining_tickets = tickets.select do |ticket|
      ticket_tags = ticket.tags.map(&:id)

      next if filter_tag(ticket, ticket_tags, 'slah-2hr-breach-notification')
      next if filter_tag(ticket, ticket_tags, 'upgrades_and_renewals')
      next if filter_tag(ticket, ticket_tags, 'renewals_gitlab_com')

      true
    end

    @logger.info "remaining ticket count #{remaining_tickets.count}"

    breach_time = Time.now.utc + SLA_BREACH_TIME

    slah_tickets = remaining_tickets.select do |ticket|
      @logger.debug '====== SLAH Tickets ======='
      sla = find_active_slah(ticket)

      next unless sla && sla.breach_at?

      if Time.at(sla.breach_at) > breach_time
        @logger.debug "ticket skipped: #{sla.breach_at} > #{breach_time} left, #{ticket.id}"
        next
      end

      true
    end

    slah_tickets
  end

  def self.filter_tag(ticket, tags, value)
    if tags.include? value
      @logger.debug "Filtering because: #{value} #{ticket.id}"
      return true
    end
    false
  end

  def self.update_tag(ticket_ids)
    @logger.info "Tickets to update #{ticket_ids}"
    zd_client.tickets.update_many!(ticket_ids, additional_tags: ['slah-2hr-breach-notification'])
  end

  def self.find_active_slah(ticket)
    # Zendesk returns a very strange Trackie format looking like this.
    # <ZendeskAPI::Trackie policy_metrics=#<Hashie::Array [
    #   <ZendeskAPI::Trackie breach_at=2020-06-01 03:13:30 UTC days=3 metric="next_reply_time" stage="active">,
    #   <ZendeskAPI::Trackie breach_at=nil metric="first_reply_time" stage="achieved">
    # ]>>
    # return only the SLA that is active (first_reply and next_reply are different)
    ticket.slas['policy_metrics'].find { |sla| sla['stage'] == 'active' }
  end
end
