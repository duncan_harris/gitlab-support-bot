# Support Bot [![Build status](https://gitlab.com/gitlab-com/support/toolbox/gitlab-support-bot/badges/master/build.svg)](https://gitlab.com/gitlab-com/support/toolbox/gitlab-support-bot/commits/master)

## Installation

```
git clone https://gitlab.com/gitlab-com/support/toolbox/gitlab-support-bot
cd gitlab-support-bot
bundle install
```

## Deployment

### Generate API tokens

**Slack**

+ https://gitlab.slack.com/apps/manage/custom-integrations
+ Click "Bots"
+ Click "Add Configuration"

**Zendesk**

To request an API token, open a [New Access Request](https://gitlab.com/gitlab-com/access-requests/issues/new?issuable_template=New%20Access%20Request) and select the **API Token Request** template from the dropdown.

### Production deployment

The production deployment of support-bot is done via the `deploy` job in `.gitlab-ci.yml`.

The following variables under - https://gitlab.com/gitlab-com/dev-resources/settings/ci_cd are used:
  + `TF_VAR_SUPPORT_BOT_SLACK`
  + `TF_VAR_SUPPORT_BOT_ZENDESK`
	+ `DEPLOY_SERVER_IP`
	+ `DEPLOY_SERVER_PASSWD`
	+ `DEPLOY_SERVER_USER`

## Slack Commands

You would use any of the commands by calling `sb <command>` in Slack.  `sb` is
the name of the configured bot in Slack.

```bash
sb help
```

**Example Output**

```
+-----------------|----------------------------------------------------------------------+
|          SUPPORTBOT HELP (gitlab-com/support/toolbox/gitlab-support-bot)               |
+-----------------|----------------------------------------------------------------------+
| COMMAND(S)      | DESCRIPTION                                                          |
+-----------------|----------------------------------------------------------------------+
| help, h         | Learn the various commands available through `sb <command>`          |
+-----------------|----------------------------------------------------------------------+
| hi              | A warm greeting                                                      |
+-----------------|----------------------------------------------------------------------+
| pods, p         | Important, Premium Gonna Breach, EE Gonna Breach < 12 hrs,           |
|                 | GitLab.com customers, All GitLab.com, and All on-prem queues         |
+-----------------|----------------------------------------------------------------------+
| self-hosted, sh | Important, Premium, and EE Gonna Breach in < 12 hours queues         |
+-----------------|----------------------------------------------------------------------+
| services, s     | GitLab.com Customers, GitLab.com queues with the total               |
|                 | number of Services tickets                                           |
+-----------------|----------------------------------------------------------------------+
```

## Development

Have (or create) a Slack bot for development, and a Zendesk token.

Create a `.env` file in the project directory and fill it with the appropriate
tokens for development:

```bash
ZD_TOKEN="xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
SLACK_API_TOKEN="xxxxxxxxxxxxxxxxxxxxxxxxxxxx"
SB_ENVIRONMENT="dev"
SLACK_RUBY_BOT_ALIASES=sbtest
```

This file is *_not_* committed and is unique to your machine.

Run the server in a new tab with `foreman start`.  Find the bot that you have created in slack using Direct Messages. Then send this development bot a PM
in Slack to try out the commands (ex: `sbtest s`).  Restart the
server when you make changes to the code.

## Troubleshooting

### Things to know

- The Digital Ocean droplet the bot runs on a CoreOS image. This was chosen as it eliminates the need to install and configure Docker.
 - **Note:** this droplet was deployed using a [terraform template that is now legacy](https://gitlab.com/gitlab-com/dev-resources/blob/master/dev-resources/gitlab-supportbot.tf) since we have a `deploy` job in CI/CD. Creating a [new droplet is on the to-do list](https://gitlab.com/gitlab-com/support/toolbox/gitlab-support-bot/issues/20). Please never update that terraform template as it will overwrite the latest deployment.

- The Docker container is configured to always restart using the `--restart always` flag at runtime.

- In `home/core` you'll find `supportbot_healthcheck.sh`, a script that inspects the container and if it is reporting unhealthy or stopped restarts it. It also writes a brief status message to `/var/log/supportbot/healthcheck.log`.

- The health check script is executed hourly using `systemd` timer. See `/etc/systemd/system/healthcheck.service` and `/etc/systemd/system/healthcheck.timer` for how it's configured.

### How do I get SSH access to the droplet?

From your terminal, do `ssh <DEPLOY_SERVER_USER>@<DEPLOY_SERVER_IP>`. When promted for a password, use the value for `DEPLOY_SERVER_PASSWD`. These values can be accessed by Maintainers and Owners in this project at https://gitlab.com/gitlab-com/support/toolbox/gitlab-support-bot/settings/ci_cd.

### It's Docker

If the bot becomes unpresponsive or goes offline, SSH into the droplet and check that the `supportbot` container is running and reporting `healthy`. If it's down, or you think a restart might be useful, do so with `docker restart supportbot`.

You can tail the logs by doing `docker logs -f supportbot` and check if the bot is communicating with Slack/Zendesk.
