############################################################
# Dockerfile build gitlab-support-bot
# https://gitlab.com/gitlab-com/support/toolbox/gitlab-support-bot
############################################################
FROM ruby:2.6.3

LABEL authors="Collen Kriel, collen@gitlab.com, Ronald van Zon, rvanzon@gitlab.com"

RUN apt-get update

RUN apt-get install -y git-core build-essential curl

RUN git clone "https://gitlab.com/gitlab-com/support/toolbox/gitlab-support-bot" /opt/gitlab-support-bot

WORKDIR /opt/gitlab-support-bot

RUN gem install bundler

RUN bundle install

HEALTHCHECK --interval=5m --timeout=3s --retries=3 \
  CMD curl -f localhost:5000 || exit 1

ENTRYPOINT ["bundle", "exec", "puma", "-p", "5000"]
